# CS371p: Object-Oriented Programming Collatz Repo

* Name: (your Full Name)
    Victor Xia
* EID: (your EID)
    vmx55
* GitLab ID: (your GitLab ID)
    VictorMXia
* HackerRank ID: (your HackerRank ID)
    victorxia
* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)
    2fe4be7cfda912ba17b19554984c690f772251ff
* GitLab Pipelines: (link to your GitLab CI Pipeline)
    https://gitlab.com/VictorMXia/cs371p-collatz/-/pipelines
* Estimated completion time: (estimated time in hours, int or float)
    10 hours
* Actual completion time: (actual time in hours, int or float)
    6 hours
* Comments: (any additional comments you have)
    N/A
