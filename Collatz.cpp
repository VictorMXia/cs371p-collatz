// Victor Xia
// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

int cache[1000000];

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// cycle_length
// ------------

// helper method for cycle length
// returns the cycle length for n
// uses lazy caching and caching of intermediate values
long long cycle_length(long long n) {
    assert(n > 0); // precondition: n must be positive
    assert(n < 1000000);
    if(cache[n] == 0) { // checking to see if n has been cached already
        long long oldN = n;
        long long count = 1;
        while(n > 1) {
            if(n % 2 == 0) { // n is even
                n = n / 2;
                if(n < 1000000) { // bounds checking
                    if(cache[n] != 0) { // next n has been cached, early return
                        cache[oldN] = count + cache[n];
                        return count + cache[n];
                    }
                }
            } else { // n is odd
                long long previousN = n;
                n = n + n/2 + 1;
                assert(previousN < n);
                ++count;
                if(n < 1000000) { // bounds checking
                    if(cache[n] != 0) { // next n has been cached, early return
                        cache[oldN] = count + cache[n];
                        return count + cache[n];
                    }
                }
            }
            ++count;
        }
        assert(count > 0); // postcondition
        cache[oldN] = count;
        int num = oldN;
        bool alreadyCached = false;
        // intermediate caching
        while(count > 0 && !alreadyCached) {
            if(num % 2 == 0 ) {
                num = num / 2;
                if(num < 1000000) { // bounds checking
                    assert(num < 1000000);
                    if(cache[num] != 0) { // exit if already cached
                        alreadyCached = true;
                    }
                    // n immediately after 1 cycle will have a cycle length of count - 1
                    cache[num] = count - 1;
                }
                --count;
            } else {
                num = (num * 3) + 1;
                if(num < 1000000) {
                    assert(num < 1000000);
                    if(cache[num] != 0) {
                        alreadyCached = true;
                    }
                    cache[num] = count - 1;
                }
                --count;
            }
        }
        return cache[oldN];
    } else {
        assert(cache[n] > 0);
        return cache[n];
    }
}

// --------------
// find_max_cycle
// --------------

// returns the max cycle length in between start and end
int find_max_cycle(int start, int end) {
    int max = 0;
    for(int i = start; i <= end; ++i) {
        int c = cycle_length(i);
        if(c > max) {
            max = c;
        }
    }
    return max;
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    int i, j;
    tie(i, j) = p;
    assert(i > 0);
    assert(j > 0);
    assert(i < 1000000);
    assert(j < 1000000);
    // <your code>
    int max = 0;
    if(i > j) {
        int m = (i/2) + 1;
        if(j < m) {
            // quiz conjecture: if j < m, max will be equal to find_max_cycle(m, i)
            max = find_max_cycle(m, i);
        } else {
            max = find_max_cycle(j, i);
        }
    } else {
        int m = (j/2) + 1;
        if(i < m) {
            max = find_max_cycle(m,j);
        } else {
            max = find_max_cycle(i, j);
        }
    }
    assert(max > 0);
    return make_tuple(i, j, max);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}
