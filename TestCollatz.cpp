// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

// ------------
// cycle_length
// ------------

TEST(CollatzFixture, cycle_length1) {
    ASSERT_EQ(cycle_length(5), 6);
}

TEST(CollatzFixture, cycle_length2) {
    ASSERT_EQ(cycle_length(10), 7);
}

TEST(CollatzFixture, cycle_length3) {
    ASSERT_EQ(cycle_length(999999), 259);
}

TEST(CollatzFixture, cycle_length4) {
    ASSERT_EQ(cycle_length(200000), 130);
}

// --------------
// find_max_cycle
// --------------

TEST(CollatzFixture, find_max_cycle1) {
    ASSERT_EQ(find_max_cycle(1, 999999), 525);
}

TEST(CollatzFixture, find_max_cycle2) {
    ASSERT_EQ(find_max_cycle(480007, 499863), 426);
}

TEST(CollatzFixture, find_max_cycle3) {
    ASSERT_EQ(find_max_cycle(235842, 360450), 441);
}

// ----
// eval
// ----


TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1, 5)), make_tuple(1, 5, 8));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(45678, 876590)), make_tuple(45678, 876590, 525));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 999999)), make_tuple(999999, 999999, 259));
}

TEST(CollatzFixture, eval8) {
    ASSERT_EQ(collatz_eval(make_pair(123456, 789012)), make_tuple(123456, 789012, 509));
}

TEST(CollatzFixture, eval9) {
    ASSERT_EQ(collatz_eval(make_pair(98765, 432109)), make_tuple(98765, 432109, 449));
}

TEST(CollatzFixture, eval10) {
    ASSERT_EQ(collatz_eval(make_pair(27, 81)), make_tuple(27, 81, 116));
}

TEST(CollatzFixture, eval11) {
    ASSERT_EQ(collatz_eval(make_pair(10, 100)), make_tuple(10, 100, 119));
}

TEST(CollatzFixture, eval12) {
    ASSERT_EQ(collatz_eval(make_pair(100, 10)), make_tuple(100, 10, 119));
}


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}
