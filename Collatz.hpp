// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param a string
 * @return a pair of ints
 */
pair<int, int> collatz_read (const string&);

// ------------
// cycle_length
// ------------

/**
 * returns the cycle length of
 * a value and caches it and its
 * intermediate values
 * @param a long long
 * @return a long long
 */
long long cycle_length(long long n);

// ------------
// find_max_cycle
// ------------

/**
 * returns the max cycle length of any value
 * between the values start and end, inclusive
 * @param two ints: start and end
 * @return an int with the max cycle length
 * calls cycle_length
 */

int find_max_cycle(int start, int end);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of ints
 * @return a tuple of three ints
 */
tuple<int, int, int> collatz_eval (const pair<int, int>&);

// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param an ostream
 * @param a tuple of three ints
 */
void collatz_print (ostream&, const tuple<int, int, int>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

#endif // Collatz_h
